                   /.7
    torn          /                       Seagate
     off         /: ST380011A
     here       /[barcode]
               /                        [fragile] ULTRA
              /ber: 5JVSGKY9            [icon   ]  ATA
             /[barcode]
            /                                 +5V 0.72A
___________/W2003-633                        +12V 0.35A
[barcode]
                                             中国产品
HDA P/N: 100348953                  Product of China
[barcode]
                                              MIC
Configuration: D14M-02
[barcode]                              E-H011-03-0084 (8)

Firmware: 8.16
[barcode]

Data Code: 06142  Site Code: WU

Caution. Product warranty is void if any seal
or label is removed, or if the drive experiences
shock in excess of 350 Gs
-----------------------------------------------------------
This drive is manufactured by Seagate for OEM
distribution. For product information or technical
support, please contact [the] system OEM.
-----------------------------------------------------------

[barcode]
CN-0MC505-21232-5A2-GKY9

[barcode]
Rev A00                                  Made in China

Options jumper block
+------+-----------------------+
| 8::: | Masteror single drive |   HDD [barcode] 
+------+-----------------------+   S/N   5JVSGKY9
| :::: | Drive is slave        |
+------+-----------------------+
| 88:: | Master with non ATA   |
|      | compatible slave      |
+------+-----------------------+
| :8:: | Cable select          |
+------+-----------------------+
| :::8 | Alternate capacity.   |
|      | Limits drive capacity |
|      | to 32 Gbytes          |
+------+-----------------------+

  7531
  ::::
  8642

-----------------------------------------------------------

                [regulatory logos]