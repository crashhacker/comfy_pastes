#makes images transparent by removing the white spaces in it
#method 1
gm convert source.png -transparent white result.png
#method 2
#d6daf0 - yotsuba color theme
convert 1.gif -coalesce -alpha set -channel RGBA -fill '#d6daf0' -opaque none out.gif
ffmpeg -i out.gif -c:v libvpx -crf 4 -b:v 0 -auto-alt-ref 0 -pix_fmt yuva420p -vf colorkey=#d6daf0 out.webm

#method 3
convert -size 1280x720 xc:transparent -background transparent \
    -channel RGBA -fill '#0FF8' \
    -draw 'polygon 200, 600, 200, 20, 600, 50, 600, 50' -fill '#0008' \
    -draw 'polygon 200, 660, 200, 40, 660, 70, 660, 70' -fill '#fFF8' \
    -draw 'polygon 200, 500, 200, 00, 500, 30, 500, 30' -channel RGBA \
    -depth 8 -blur '10x5' test.png

convert -size 1280x720 xc:yellow -background yellow -channel RGBA gnd.png
ffmpeg -loop 1 -i test.png -t 1 -pix_fmt argb -vcodec qtrle z.mov
ffmpeg -loop 1 -i gnd.png -t 1 -pix_fmt argb -vcodec qtrle gnd.mov
ffmpeg -vcodec qtrle -i gnd.mov -vcodec qtrle -i z.mov \
    -filter_complex "[0:0][1:0]overlay=format=rgb[out]" -shortest \
    -map [out] -vcodec qtrle test.mov

#method 3
for f in *.{mp4,mkv,gif,png,jpg};
do ffmpeg -i "$f" -an -c:v libvpx -auto-alt-ref 0 -pix_fmt yuva420p "out/${f%.*}.webm"; done


#search keywords in pdfs
KEYWORD="$1"
pdfgrep -R "$KEYWORD" ./

#get top processes based on their cpu usage (way lighter than htop)
ps aux | awk '{print $2, $4, $11}' | sort -k2rn | head -n 20

#download images/ppts, pdfs etc from 4chan/8chan threads
#!/bin/bash
exts=(webm mp4 ppt pptx docx zip tar.gz gif png jpg pdf mp3 sh)
tmpfile=".urls.tmp"
lynx -dump -listonly -nonumbers $1 | sort -u > "$tmpfile"
for ext in ${exts[*]}; do grep "\.$ext$" "$tmpfile" | tee -a $ext\_links.txt | aria2c -i - -d "$ext"; done
rm "$tmpfile"

#clear cache in system (needs sudo obv)
echo 1 > /proc/sys/vm/drop_caches
echo 2 > /proc/sys/vm/drop_caches
echo 3 > /proc/sys/vm/drop_caches
sync
sudo sync & sysctl -w vm.drop_caches=3